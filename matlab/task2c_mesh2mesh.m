% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Learning Where To Classify In Multi-View Semantic Segmentation
% ETHZ RueMonge 2014 dataset + load / evaluation code
%
% Learning Where To Classify In Multi-View Semantic Segmentation
% ECCV 2014, Zurich, Switzerland. (PDF, poster, project)
% H. Riemenschneider, A. Bodis-Szomoru, J. Weissenberg, L. Van Gool
%
% http://varcity.eu/3dchallenge/
% http://www.vision.ee.ethz.ch/~rhayko/paper/eccv2014_riemenschneider_multiviewsemseg/
%
% Please cite the above work if you use any of the code or dataset.
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% TASK 2 - Mesh Labelling - collect or reason in 3d to label mesh
% SUMMARY

% THREE MODES
% 2a - Image2Mesh Labelling - collect images to label mesh 
% 2b - Pointcloud2Mesh Labelling - collect pointcloud to label mesh
% 2c - Mesh2Mesh Labelling - collect mesh results directy (THIS CODE)

% YOUR OWN TRAINING HERE?
%   see example gmm script
%
% YOUR OWN 3D MESH LABELING?
%   dataset.mesh_predict == [1 x num_face] labels ranging from [0...num_label]
%   score = evaluation_multilabel(dataset.mesh_gt_test-1, dataset.mesh_predict-1, dataset_labelskip)

close all
clear
clc


%% SETUP PATHS
addpath(genpath('.'))
%addpath(genpath('thirdparty/gco'))

method = 'mesh2mesh'; %  LABELLED MESH
dataset = load_dataset('ruemonge428','results_eccv2014', method);

%tabulate(dataset.mesh_gt_test)
%tabulate(dataset.mesh_gt_train)


%% DATA COLLECTION
method = 'images'; %  RAW IMAGES

if(~exist(dataset.predicttestfiledata,'file'))
    display(['processing ' dataset.predicttestfiledata] )
    t=cputime;
    data = mesh_collect_files (dataset, dataset.all.list_idx, dataset.all.num_view, dataset.num_label, dataset.num_face, dataset.cm, method);
    timing.load=cputime-t;
    save(dataset.predicttestfiledata,'data','-v7.3')
else
    display(['loading ' dataset.predicttestfiledata] )
    %t=cputime;
    load(dataset.predicttestfiledata,'data')
    %timing.loaddata=cputime-t;
end



%% BUILD GMM CLASSIFIER

data.cams = squeeze(max(sum(data.col>0,1),[],2))';
data.colmean = bsxfun(@times, squeeze(sum(data.col,1)), 1./max(data.cams,1));
data.classmean = zeros(dataset.num_label, 3);
data.classstd = zeros(dataset.num_label, 3);

% test
for labidx = 1:dataset.num_label
    data.classmean(labidx,:) = mean(data.colmean(:, dataset.mesh_gt_test==labidx),2);
    data.classstd(labidx,:) = std(data.colmean(:, dataset.mesh_gt_test==labidx),[],2);
end
data.classmean

% train
for labidx = 1:dataset.num_label
    %data.classmean(labidx,:) = mean(data.colmean(:, dataset.mesh_gt_train==labidx),2);
    %data.classstd(labidx,:) = std(data.colmean(:, dataset.mesh_gt_train==labidx),[],2);
end
data.classmean



data.predictprob_sum = zeros(dataset.num_label, dataset.num_face);
for labidx = 1:dataset.num_label
    for dim = 1:3
    normmax = normpdf(data.classmean(labidx,dim),data.classmean(labidx,dim), data.classstd(labidx,dim));
    norm = normpdf(data.colmean(dim,:),data.classmean(labidx,dim), data.classstd(labidx,dim)) / normmax;
    data.predictprob_sum(labidx,:) = data.predictprob_sum(labidx,:) + 1/3*norm;
    end
end

mean(data.predictprob_sum,2)'
std(data.predictprob_sum,[],2)'

dataset.unary = conversion_likelihood2cost(data.predictprob_sum, false);

% gmm better on training data than training on test data!?
%    [15.1134 53.2269 6.1697 7.9708 15.9967 46.5912 15.3358] % train 
%    [9.2559 50.2314 5.2989 4.0652 11.6993 59.2625 8.6236] % test


    
%% GENERATE 3D GRAPH RESULT
display(['optimizing graph labelling...'])

dataset.alpha = 0.0; % MAP MAX result (without any regularization)
%dataset.alpha = 0.1; % GRAPHCUT regularization

t=cputime;
dataset.pairwise = mesh_adjacency_onehit(dataset.face, dataset.vertex);
dataset.mesh_predict = tool_graphcut_gco(dataset.unary, dataset.pairwise, dataset.alpha, [], [], true, 100000)';
timing.optimization=cputime-t;




%% EVALUATION TASK 2c - Mesh2Mesh Labelling - collect mesh results directy (THIS CODE)
display(['evaluating results...'])

tabulate(dataset.mesh_gt_test)
tabulate(dataset.mesh_predict)

t=cputime;
score = evaluation_multilabel(dataset.mesh_gt_test-1, dataset.mesh_predict-1, dataset.labelskipidx, dataset.num_label);
timing.evaluation=cputime-t;


display(['exporting results...'])
t=cputime;
score
evaluation_confusion(score.confusion, dataset.labels_str_used, [dataset.name ' ' method]);
saveas(gcf,[dataset.predicttestfilescore(1:end-4) '.png'])

mesh_predict = dataset.mesh_predict;
save(dataset.predicttestfilescore,'score','timing','mesh_predict')
export_ply_simple(dataset.predicttestfileply, dataset.vertex, dataset.face, dataset.cm(mesh_predict,:)'*255)
timing.export=cputime-t;


display(['done!'])
diary off;
