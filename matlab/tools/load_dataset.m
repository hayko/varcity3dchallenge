function dataset = load_dataset(datasetname, result, method)
% dataset = load_dataset(base, datasetname, result)
%   setup dataset structure
%
% author: hayko riemenschneider, 2014-2015
%



%% SETUP PATHS

dataset.name = datasetname;
dataset.base = ['../data/' datasetname '/'];
dataset.result = result;

dataset.mesh = fullfile(dataset.base, 'mesh.mat');

dataset.images = fullfile(dataset.base, 'image','/');
dataset.index = fullfile(dataset.base, 'index','/');
dataset.train.gt = fullfile(dataset.base, 'label','/');
dataset.test.gt = fullfile(dataset.base, 'label','/');
dataset.train.gtfile = fullfile(dataset.base, 'mesh_gt_train.mat');
dataset.test.gtfile = fullfile(dataset.base, 'mesh_gt_test.mat');

dataset.mesh_gt_test = load(dataset.test.gtfile, 'mesh_gt_test');
dataset.mesh_gt_train = load(dataset.train.gtfile, 'mesh_gt_train');
dataset.mesh_gt_test = dataset.mesh_gt_test.mesh_gt_test; % fix
dataset.mesh_gt_train = dataset.mesh_gt_train.mesh_gt_train; % fix

dataset.mesh_gt_train_idx = find(dataset.mesh_gt_train>1); % non-void label samples
dataset.mesh_gt_test_idx = find(dataset.mesh_gt_test>1); % non-void label samples


% dataset.imageprob2mesh = fullfile(dataset.base, dataset.result, 'imageprob2mesh','/'); % YOUR RESULTS - raw probabilities in matlab
% dataset.imagemap2mesh = fullfile(dataset.base, dataset.result, 'imagemap2mesh','/'); % YOUR RESULTS - MAP labelling
% dataset.imagegco2mesh = fullfile(dataset.base, dataset.result, 'imagegco2mesh','/'); % YOUR RESULTS - GRAPHCUT labelling
% 
% dataset.mesh2mesh = fullfile(dataset.base, dataset.result, 'mesh2mesh','/'); % YOUR RESULTS - 3D MESH labelling
% 
% %%
% 
% switch(method)
%     case {'imageprob2mesh','imageprob2image'}
%     dataset.predicttest = dataset.imageprob2mesh; % USE RAW result folder
%     case {'imagegco2mesh','imagegco2image'}
%     dataset.predicttest = dataset.imagegco2mesh; % USE GCO result folder
%     case {'imagemap2mesh','imagemap2image'}
%     dataset.predicttest = dataset.imagemap2mesh; % USE MAP result folder
%     case 'mesh2mesh'
%     dataset.predicttest = dataset.mesh2mesh; % USE MAP result folder
% 
%     case 'images'
%     dataset.predicttest = dataset.mesh2mesh; % USE MAP result folder
% end

dataset.predicttest = fullfile(dataset.base, dataset.result, method, '/'); 

dataset.predicttestfiledata = [dataset.predicttest(1:end-1) '_data.mat'];
dataset.predicttestfilescore = [dataset.predicttest(1:end-1) '_score.mat'];
dataset.predicttestfileply = [dataset.predicttest(1:end-1) '.ply'];
dataset.predicttestfilediary = [dataset.predicttest(1:end-1) '.log'];


fprintf('logging to  %s\n',dataset.predicttestfilediary);
if exist(dataset.predicttestfilediary,'file'); delete(dataset.predicttestfilediary); end
diary(dataset.predicttestfilediary);
diary on;
%fprintf('Running script %s.m\n',mfilename('fullpath'));
%fprintf('Host: '); system('hostname'); 

display(['loading ' dataset.name])
display(['processing' dataset.predicttest])

%% LOAD DATA FILE NAMES & INDEX
%filelist_all = 'listall.txt';
%filelist_train = 'listtrain.txt';
%filelist_eval = 'listeval.txt';

dataset.all.list_idx = load_listindex(fullfile(dataset.base, 'list_full', 'listall.txt'));
dataset.train.list_idx = load_listindex(fullfile(dataset.base, 'list_full', 'listtrain.txt'));
dataset.test.list_idx = load_listindex(fullfile(dataset.base, 'list_full', 'listeval.txt'));

dataset.all.num_view = length(dataset.all.list_idx);
dataset.train.num_view = length(dataset.train.list_idx);
dataset.test.num_view = length(dataset.test.list_idx);


%% LOAD MESH AND LABELS

load(dataset.mesh, 'cm','face','vertex','labelskipidx','labels_str'); 
face = double(face);

dataset.labelskipidx = labelskipidx;

% all labels including currently unused
dataset.labels_str = labels_str;
% clean actually only used labels
dataset.labels_str_used = dataset.labels_str;
dataset.labels_str_used(dataset.labelskipidx) = [];

dataset.cm = cm;
dataset.face = face;
dataset.vertex = vertex;

dataset.num_face =  size(face,2);
dataset.num_vertex =  size(vertex,2);
dataset.num_label = size(cm,1);

display(['found ' num2str(dataset.num_label) ' labels in colormap'])






