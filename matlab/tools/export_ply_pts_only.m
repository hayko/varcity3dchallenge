function export_ply_pts_only(filename, vertex, vertex_color)
% export_ply_simple(filename, vertex, vertex, vertex_color)
%  A simple PLY writer from vertex and color data to a .PLY file
%
% author: hayko riemenschneider, 2014

numVertices =  size(vertex,2);

% write ply header
fid = fopen(filename,'w');
fprintf(fid,'ply\n');
fprintf(fid,'format ascii 1.0\n');
fprintf(fid,'element vertex %d\n', numVertices);
fprintf(fid,'property float x\n');
fprintf(fid,'property float y\n');
fprintf(fid,'property float z\n');
fprintf(fid,'property uchar red\n');
fprintf(fid,'property uchar green\n');
fprintf(fid,'property uchar blue\n');
fprintf(fid,'end_header\n');

% write ply data
fprintf(fid, '%f %f %f %d %d %d \n', [vertex; (round(vertex_color))]);
fclose(fid);
