function [labelmap, label_str, label_skipidx] = colormap_zurich2rgb (visualize)

label_str = {'void','sky','water','vegetation','groundroad','wall','roof','window','shutter','door','balcony','shop'};
label_skipidx = [];

% design by matrix and hiarchy

% NAME:     Void Window Wall Balcony Door Roof Sky Shop Shutter ROAD
% LABELMAP  0  1       2       3      4      5   6   7     8    9
% MATLAB    1  2       3       4      5      6   7   8     9   10


% pastel color options     32          64          96      128       160       192         224   256
% pastel color options    0.1250    0.2500    0.3750    0.5000    0.6250    0.7500    0.8750    1.0000
% R  G  B
labelmap = [
 192 192 192;   % void          lightgray
 160 224 256;   % sky           cyan blue
 96 96 256;     % water         dark blue
 96 160  96;    % vegetation    greeniesh
 128 128 128;   % groundroad    gray
 256 256 96;    % wall          yellow
 224 160 96;    % roof          orange
 256 96 96;     % window        redish
 160 160 96;    % windowshutter brown
 128 96 96;     % door          dark brown
 128 0 256;     % balcony       
 128 192 64;     % shop
 
]/256;


if(~exist('visualize','var')), visualize = false; end;

if(visualize)
    figure
    im_labelmap = viz_labelmap(labelmap, label_str);
    imshow (im_labelmap)
%     im_labels = imresize(repmat((1:size(labelmap,1))',1,10), 50, 'nearest');
%    imshow(label2rgb(im_labels,labelmap))
%     for idx = 1: size(labelmap, 1)
%         text(200,25+50*(idx-1),label_str{idx},'Color','k','FontSize', 20)
%     end
%    saveas(gcf,[mfilename '.png'])
    imwrite(im_labelmap,[mfilename '.png'])
        
end
